﻿using UnityEngine;

namespace test.player
{
    public class PlayerLook : MonoBehaviour
    {
        [SerializeField] private Transform headTransform;
        [SerializeField] private Transform orientation;
        [SerializeField] private float angleLimit = 90f;
        [Header("Mouse")] [SerializeField] private float sensitivity = 1f;
        [SerializeField] private float senMultiplier = 1f;

        private float _pitch;
        private float _yaw;

        private float _rotationX;
        private float _rotationY;

        private void Awake()
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        private void Update()
        {
            MyInput();
            Look();
        }

        private void MyInput()
        {
            _pitch = Input.GetAxisRaw("Mouse Y");
            _yaw = Input.GetAxisRaw("Mouse X");
        }

        private void Look()
        {
            _rotationX -= _pitch * sensitivity * Time.fixedDeltaTime * senMultiplier;
            _rotationX = Mathf.Clamp(_rotationX, -angleLimit, angleLimit);
            headTransform.localRotation = Quaternion.Euler(_rotationX, 0f, 0f);

            _rotationY = _yaw * sensitivity * Time.fixedDeltaTime * senMultiplier;
            orientation.Rotate(Vector3.up * _rotationY);
        }
    }
}