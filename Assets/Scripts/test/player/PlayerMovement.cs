﻿using UnityEngine;

namespace test.player
{
    public class PlayerMovement : MonoBehaviour
    {
        [Header("Player")] [SerializeField] private float playerHeight = 2f;
        [SerializeField] private Transform orientation;

        [Header("Movement")] [SerializeField] private float moveSpeed = 50;
        [SerializeField] private float shiftSpeed = 20;
        [SerializeField] private float movementMultiplier = 1f;
        [SerializeField] private float airMultiplier = 0.3f;
        [SerializeField] private float jumpPower = 20f;
        [SerializeField] private float groundDrag = 6f;
        [SerializeField] private float airDrag = 5f;

        [Header("Ground Check")] [SerializeField]
        private float maxSlopeAngle = 45f;

        [Header("Ground Check")] [SerializeField]
        private Transform groundCheckTransform;

        [SerializeField] private float groundCheckRadius = 0.05f;
        [SerializeField] private LayerMask groundLayerMask;

        private Rigidbody _rb;

        private float _vertical;
        private float _horizontal;
        private bool _isJumpPress;
        private bool _isGrounded;
        private bool _isShift;

        private Vector3 _moveDirection;
        private Vector3 _slopeMoveDirection;

        private void Awake()
        {
            _rb = GetComponent<Rigidbody>();
            _rb.freezeRotation = true;
        }

        private void Update()
        {
            PlayerInput();
            ControlDrag();
            GroundCheck();
        }

        private void FixedUpdate()
        {
            ApplyJump();
            ApplyVelocity();
        }

        private bool OnSlope()
        {
            if (!Physics.Raycast(transform.position, Vector3.down, out var slopeHit, playerHeight / 2f + 0.05f))
                return false;
            if (!IsFloor(slopeHit.normal)) return false;
            _slopeMoveDirection = Vector3.ProjectOnPlane(_moveDirection, slopeHit.normal);
            return true;
        }

        private bool IsFloor(Vector3 normal)
        {
            return Vector3.Angle(Vector3.up, normal) < maxSlopeAngle;
        }

        private void GroundCheck()
        {
            _isGrounded = Physics.CheckSphere(groundCheckTransform.position, groundCheckRadius, groundLayerMask);
        }

        private void ControlDrag()
        {
            _rb.drag = _isGrounded ? groundDrag : airDrag;
        }

        private void PlayerInput()
        {
            _isJumpPress = Input.GetButton("Jump");
            _isShift = Input.GetKey(KeyCode.LeftShift);
            _vertical = Input.GetAxisRaw("Vertical");
            _horizontal = Input.GetAxisRaw("Horizontal");
            _moveDirection = orientation.forward * _vertical + orientation.right * _horizontal;
        }

        private void ApplyJump()
        {
            if (!_isGrounded || !_isJumpPress) return;
            Vector3 velocity = _rb.velocity;
            _rb.velocity = new Vector3(velocity.x, 0f, velocity.z);
            _rb.AddForce(Vector3.up * jumpPower, ForceMode.Impulse);
        }

        private void ApplyVelocity()
        {
            //Extra gravity By Dani
            _rb.AddForce(Vector3.down * Time.fixedDeltaTime * 10f);

            if (_isGrounded)
            {
                float speed = _isShift ? shiftSpeed : moveSpeed;
                //On ground
                if (OnSlope())
                {
                    _rb.AddForce(_slopeMoveDirection.normalized * speed * movementMultiplier,
                        ForceMode.Acceleration);
                }
                else
                {
                    _rb.AddForce(_moveDirection.normalized * speed * movementMultiplier, ForceMode.Acceleration);
                }
            }
            else
            {
                //Not on ground
                _rb.AddForce(_moveDirection.normalized * moveSpeed * movementMultiplier * airMultiplier,
                    ForceMode.Acceleration);
            }
        }
    }
}