using Mirror;
using UnityEngine;

namespace test.testmultiplayer
{
    public class TestPlayer : NetworkBehaviour
    {
        private Camera _cam;

        private void Awake()
        {
            _cam = Camera.main;
        }

        private void Update()
        {
            if (!isLocalPlayer) return;

            //Hit
            if (Input.GetMouseButtonDown(1))
            {
                Debug.Log("Kick!");
                if (!Physics.Raycast(_cam.ScreenPointToRay(Input.mousePosition), out var hit)) return;
                if (!hit.transform.TryGetComponent<IDamageable>(out var damageable)) return;
                damageable.Damage();
            }

            if (Input.GetKeyDown(KeyCode.H))
            {
                HealSelf(10);
            }
        }

        public int health;

        [Command]
        private void HealSelf(int amount)
        {
            health += amount;
            TargetHealthMe(amount);
        }

        [TargetRpc]
        private void TargetHealthMe(int h)
        {
            Debug.Log($"Healed You +{h}");
        }
    }
}