﻿namespace test.testmultiplayer
{
    public interface IDamageable
    {
        public void Damage();
    }
}