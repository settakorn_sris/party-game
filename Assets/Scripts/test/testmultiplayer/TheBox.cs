using Mirror;
using UnityEngine;

namespace test.testmultiplayer
{
    public class TheBox : NetworkBehaviour, IDamageable
    {
        [SerializeField] private GameObject boxDestroyedPrefab;

        private void Awake()
        {
            NetworkClient.RegisterPrefab(boxDestroyedPrefab);
        }

        public enum BoxState : byte
        {
            NORMAL,
            DESTROYED
        }

        [SyncVar(hook = nameof(BoxStateChanged))]
        public BoxState boxState = BoxState.NORMAL;

        private void BoxStateChanged(BoxState _, BoxState newState)
        {
            if (newState == BoxState.DESTROYED)
            {
                Destroy(gameObject);

                GameObject box = Instantiate(boxDestroyedPrefab);
                box.transform.position = transform.position;
                box.transform.rotation = transform.rotation;
                NetworkServer.Spawn(box);
            }
        }

        [Client]
        public void Damage()
        {
            SetBoxState();
        }

        [Command(requiresAuthority = false)]
        private void SetBoxState(NetworkConnectionToClient sender = null)
        {
            Debug.Log($"Destroyed by {sender.identity.name}");
            boxState = BoxState.DESTROYED;
        }
    }
}